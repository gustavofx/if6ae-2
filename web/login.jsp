<%-- 
    Document   : formulario
    Created on : 24/09/2015, 13:50:29
    Author     : Gustavo
--%>
<?xml version="1.0" encoding="UTF-8"?>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Login</title>
      <style>
          #error {
              color: red;
          }
          #success {
              color: blue;
          }
      </style>
   </head>
   <body>          
       <jsp:useBean id="pessoa" class="pratica.jsp.loginBean" scope="session"></jsp:useBean>
       <jsp:setProperty name="pessoa" property="*"/>
      <form method="post" action="/pratica-jsp/login.jsp">
         Código: <input id="login" type="text" name="login"/><br/>
         Nome: <input id="senha" type="password" name="senha"/><br/>
         Perfil: <select id="perfil" name="perfil">
                     <option value="1">Cliente</option>
                     <option value="2">Gerente</option>
                     <option value="3">Administrador</option>
                 <select>
         <input type="submit" value="Enviar"/> 
         <% 
             String success = "";
             String error = "";
            if (request.getMethod().equals("POST")) {
                if(pessoa.getLogin().equals(pessoa.getSenha())){                        
                    Date hora = new Date();
                    int p = Integer.parseInt(pessoa.getPerfil());
                    String perfil = "";
                    switch(p){
                        case 1:
                            perfil = "Cliente";
                            break;
                        case 2:
                            perfil = "Gerente";
                            break;
                        case 3:
                            perfil = "Administrador";
                            break;
                    }
                    success = perfil+", login bem sucedido, para "+pessoa.getLogin()+" às "+hora;
                }else{
                    error = "Acesso negado";
                }                                        
            } 
        %>
         <div id="success">
             <%= success %>
         </div>
         <div id="error">
             <%= error %>
         </div>
      </form>
   </body>
</html>

         
